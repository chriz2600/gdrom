module test;

  /* Make a reset that pulses once. */
  reg reset = 0;
  initial begin
     # 200 $finish;
  end

  /* Make a regular pulsing clock. */
  reg clk = 0;
  reg clkout_cpu = 0;
  reg clkout_cpu2 = 0;

  always #5 clk = !clk;

  wire [7:0] value;

  assign cs_gate = ~clkout_cpu&~clkout_cpu2;

   always @(posedge clk) begin
      clkout_cpu2 <= ~clkout_cpu2;
      if (~clkout_cpu2)
        clkout_cpu <= ~clkout_cpu;
   end

  initial
     $monitor("At time %t, %b %b %b %b", $time, clk, clkout_cpu2, clkout_cpu, cs_gate);
endmodule // test