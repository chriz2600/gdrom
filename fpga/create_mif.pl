#!/usr/bin/perl -w

use strict;

my $count = 0;

print "DEPTH = 4096;\n";
print "WIDTH = 8;\n";
print "ADDRESS_RADIX = DEC;\n";
print "DATA_RADIX = HEX;\n";
print "CONTENT\n";
print "BEGIN\n";
while (<>) {
    my $line = $_;
    chomp($line);
    print $count . " : " . $line . ";\n";
    last if ($count == 4095);
    $count += 1;
}
print "END;\n";
