


create_clock -period 12Mhz -name inclk [get_ports clk]
derive_pll_clocks -create_base_clocks
derive_clock_uncertainty
